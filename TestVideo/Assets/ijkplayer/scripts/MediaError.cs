﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace dpn
{
    public static class MediaError
    {
        public const int MEDIA_ERROR_UNKNOWN = 1;
        public const int MEDIA_ERROR_SERVER_DIED = 100;
        public const int MEDIA_ERROR_NOT_VALID_FOR_PROGRESSIVE_PLAYBACK = 200;
        public const int MEDIA_ERROR_IO = -1004;
        public const int MEDIA_ERROR_MALFORMED = -1007;
        public const int MEDIA_ERROR_UNSUPPORTED = -1010;
        public const int MEDIA_ERROR_TIMED_OUT = -110;
        public const int MEDIA_ERROR_IJK_PLAYER = -10000;
        public const int MEDIA_ERROR_TOO_MANY_RELOADTIMES = -10086;

        public const int DESCRIPTION_MEDIASOURCE_ERROR = 1001;
        public const int DESCRIPTION_UNKNOW_ERROR = 1000;
    }
}
