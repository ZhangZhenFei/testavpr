using UnityEngine;
using System.Collections;
using System;
using UnityEngine.UI;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using System.Runtime.InteropServices;
using LitJson;
using System.Collections.Generic;

namespace dpn
{
    public class TrackInfo
    {
        public const String TRACK_TYPE_VIDEO = "video";
        public const String TRACK_TYPE_AUDIO = "audio";
        public const String TRACK_TYPE_UNKNOW = "unknown";
        public int index = -1;
        public String type = TRACK_TYPE_UNKNOW;
        public String codec = "unknown";
        public String title = "unknown";
        public TrackInfo(JsonData JsonString)
        {
            SetFromString(JsonString);
        }

        public TrackInfo()
        {

        }

        public void SetFromString(JsonData info)
        {

            try
            {
                this.index = int.Parse(info["index"].ToString());
                this.type = info["type"].ToString();
                switch (this.type)
                {
                    case TRACK_TYPE_VIDEO:
                        this.codec = info["codec"].ToString();
                        break;
                    case TRACK_TYPE_AUDIO:
                        this.title = info["title"].ToString();
                        break;
                    default:
                        break;
                }
            }
            catch (Exception e)
            {
                Debug.Log(e.ToString());
            }

        }

        public void DumpInfo()
        {
            Debug.Log("current track info index :" + this.index + " type: " + this.type + " codec:" + this.codec + " title: " + this.title);
        }

    }
}
