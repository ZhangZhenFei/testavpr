using UnityEngine;
using System.Collections;
using System;
using UnityEngine.UI;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using System.Runtime.InteropServices;
using System.Collections.Generic;
using System.Linq;

namespace dpn
{
    public class P1ProCodecSelector : CodecSelector
    {
        private string TAG = "P1ProCodecSelector";
        public P1ProCodecSelector()
        {
            InitDecoderCapbility();
        }

        public void InitDecoderCapbility()
        {
            DecoderCapbility.Instance().AddToBlackList("video/mpeg2_1_4", IjkPlayerProxy.DECODER_MEDIACODEC);
            DecoderCapbility.Instance().AddToBlackList("video/mp4v-es_0_3", IjkPlayerProxy.DECODER_MEDIACODEC);
        }

        public int GetDefaultDPVRDecoder()
        {
            return IjkPlayerProxy.DECODER_MEDIACODEC;
        }

        private bool CheckDecoderSupport(int decoder, string videoCodec, string audioCodec)
        {
            return DecoderCapbility.Instance().CheckIsEnableToDecode(decoder, videoCodec) && DecoderCapbility.Instance().CheckIsEnableToDecode(decoder, audioCodec);
        }
        public int GetBestDecoder(MediaInfo info)
        {
            int bestDecoder = IjkPlayerProxy.DECODER_UNKNOWN;
            if (!info.gotInfo)
            {
                return bestDecoder;
            }

            string ac = info.audioCodec;
            string vc = info.videoCodec;
            int width = info.videoWidth;
            int height = info.videoHeight;

            if (width > 4096)
            {
                return -1;
            }

            Dictionary<int, int> scoreMap = new Dictionary<int, int>();
            scoreMap.Add(IjkPlayerProxy.DECODER_MEDIACODEC, 5);
            scoreMap.Add(IjkPlayerProxy.DECODER_FFMPEG, 0);

            if (vc.Contains("hevc"))
            {
                scoreMap.Remove(IjkPlayerProxy.DECODER_FFMPEG);
            }

            List<int> unsupportCodecs = new List<int>();
            foreach (var item in scoreMap)
            {
                if (!CheckDecoderSupport(item.Key, vc, ac))
                {
                    unsupportCodecs.Add(item.Key);
                }
            }

            foreach (var key in unsupportCodecs)
            {
                scoreMap.Remove(key);
            }

            if (scoreMap.Count > 0)
            {
                bestDecoder = scoreMap.First(q => q.Value == scoreMap.Values.Max()).Key;
            }

            /* in case media codec resize error */
            if (width * height < 200 * 100)
            {
                bestDecoder = IjkPlayerProxy.DECODER_FFMPEG;
            }
            /* special filter for dpvr_bd_film, fix me */
            else if (width * height < 2000 * 1100)
            {
                if (vc.Equals("video/avc_100_40"))
                {
                    bestDecoder = IjkPlayerProxy.DECODER_FFMPEG;
                }
            }
            return bestDecoder;
        }
    }
}
