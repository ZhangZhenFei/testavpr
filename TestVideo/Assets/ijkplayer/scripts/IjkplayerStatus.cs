﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

namespace dpn
{

    public class IjkplayerStatus : MonoBehaviour
    {

        public Text StatusText;
        public IjkPlayer Player;
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {
            if (StatusText == null || Player == null || Player.IsReady == false)
            {
                return;
            }

            string text = "";

            text += "playing: " + Player.IsPlaying + "\n";
            text += "duration: " + Player.Duration + "\n";
            text += "current: " + Player.Current + "\n";
            text += "buffered: " + (float)Player.Buffered + "(" + (Player.Buffered * 100.0f / Player.Duration) + "%)" + "\n";

            StatusText.text = text;
        }
    }
}
