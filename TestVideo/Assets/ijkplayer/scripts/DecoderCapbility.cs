﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using UnityEngine;

namespace dpn
{
    public class DecoderCapbility
    {
        Dictionary<string, Codec> capbiltyMap;
        Dictionary<string, int> forceDecoderMap;
        public List<string> HWSupportCodec;
        public List<string> HWUnsupportMuxer;

        public static String CHANNEL_LAYOUT_STEREO = "stereo";
        public static String CHANNEL_LAYOUT_MONO = "mono";

        class Codec
        {
            public string codecName;
            public List<int> disableDecoders;

            public Codec(String codecName)
            {
                this.codecName = codecName;
                disableDecoders = new List<int>();
            }

            public bool IsAviable(int decoder)
            {
                return disableDecoders.Contains(decoder) ? false : true;
            }

            public void SetDisableDecoder(int decoder)
            {
                if (!disableDecoders.Contains(decoder))
                {
                    disableDecoders.Add(decoder);
                }
            }

            public void RemoveDisableDecoder(int decoder)
            {
                if (disableDecoders.Contains(decoder))
                {
                    disableDecoders.Remove(decoder);
                }
            }
        }

        private static DecoderCapbility instance = null;
        static DecoderCapbility()
        {
            instance = new DecoderCapbility();
        }

        public static DecoderCapbility Instance()
        {
            return instance;
        }

        private DecoderCapbility()
        {
            capbiltyMap = new Dictionary<string, Codec>();
            forceDecoderMap = new Dictionary<string, int>();
            HWSupportCodec = new List<string>();
            HWUnsupportMuxer = new List<string>();
            //   LoadDefaultCodecSettings();
        }

        private void LoadDefaultCodecSettings()
        {
            /* vr 9 setting */
            AddToBlackList("video/mp4v-es_0_1", IjkPlayerProxy.DECODER_MEDIACODEC);
            AddToBlackList("ac3", IjkPlayerProxy.DECODER_ANDROID_MEDIAPLAYER);
            //AddToBlackList("video/avc_77_31", IjkPlayerProxy.DECODER_ANDROID_MEDIAPLAYER);
            //AddToBlackList("video/avc_77_31", IjkPlayerProxy.DECODER_MEDIACODEC);
            AddToForceDecoder("video/x-vnd.on2.vp9_0_1", IjkPlayerProxy.DECODER_ANDROID_MEDIAPLAYER);
            AddToForceDecoder("video/wmv1_0_1", IjkPlayerProxy.DECODER_FFMPEG);
            AddToForceDecoder("video/wmv2_0_1", IjkPlayerProxy.DECODER_FFMPEG);

            HWSupportCodec.Add("avc");
            HWSupportCodec.Add("hevc");
            HWSupportCodec.Add("aac");

            HWUnsupportMuxer.Add("ts");
        }

        public void AddToBlackList(string codecName, int decoder)
        {
            if (!capbiltyMap.ContainsKey(codecName))
            {
                Codec c = new Codec(codecName);
                c.SetDisableDecoder(decoder);
                capbiltyMap.Add(codecName, c);
            }
            else
            {
                Codec c = capbiltyMap[codecName];
                c.SetDisableDecoder(decoder);
            }
        }

        /* video codec only */
        public void AddToForceDecoder(string codecName, int decoder)
        {
            if (forceDecoderMap.ContainsKey(codecName))
            {
                forceDecoderMap[codecName] = decoder;
            }
            else
            {
                forceDecoderMap.Add(codecName, decoder);
            }
        }

        public bool CheckHWMutexSupport(string mediaUrl)
        {
            bool result = !HWUnsupportMuxer.Contains(GetFileExtenxion(mediaUrl).ToLower());
            return result;
        }

        public static string GetFileExtenxion(string filePath)
        {
            string extension = "";
            int lastIndex = filePath.LastIndexOf(".");
            if (lastIndex != -1)
                extension = filePath.Substring(lastIndex + 1, filePath.Length - lastIndex - 1);
            return extension;
        }
        public int CheckForceDecoder(string vc, string ac, string mediaUrl, bool isOnline)
        {

            if ((!isOnline && CheckHWMutexSupport(mediaUrl)) ||
                 (isOnline && mediaUrl.Contains("m3u8")))
            {
                int codecPass = 0;
                foreach (string codec in HWSupportCodec)
                {
                    if (((vc != null) && vc.Contains(codec)) ||
                        ((ac != null) && ac.Contains(codec)))
                    {
                        codecPass++;
                        if (codecPass == 2)
                        {
                            return IjkPlayerProxy.DECODER_ANDROID_MEDIAPLAYER;
                        }
                    }
                }
            }

            return forceDecoderMap.ContainsKey(vc) ? forceDecoderMap[vc] : -1;
        }

        public Boolean CheckIsEnableToDecode(int decoder, string codec)
        {
            if ((codec != null) && capbiltyMap.ContainsKey(codec) && !capbiltyMap[codec].IsAviable(decoder))
            {
                return false;
            }
            return true;
        }

        public void ImportSettingFromJson(string configJson)
        {
            if (configJson == null)
            {
                Debug.Log("Import a null config to init blacklist");
                return;
            }
            capbiltyMap.Clear();
            Dictionary<string, object> map = LitJson.JsonMapper.ToObject<Dictionary<string, object>>(configJson);
            foreach (string key in map.Keys)
            {
                List<object> decoders = map[key] as List<object>;
                for (int i = 0; i < decoders.Count; i++)
                {
                    AddToBlackList(key, Int32.Parse(decoders[i].ToString()));
                }
            }
            return;
        }

        public string DumpBlackList()
        {
            string retStr = "";
            foreach (string key in capbiltyMap.Keys)
            {
                foreach (int i in capbiltyMap[key].disableDecoders)
                {
                    retStr += "\nCodec : " + key + " black list:" + i;
                }
            }
            return retStr;
        }

        public string ExportCurrentSettingToJson()
        {
            Dictionary<string, List<int>> map = new Dictionary<string, List<int>>();
            foreach (string key in capbiltyMap.Keys)
            {
                map.Add(key, capbiltyMap[key].disableDecoders);
            }
            return LitJson.JsonMapper.ToJson(map);
        }

    }
}
