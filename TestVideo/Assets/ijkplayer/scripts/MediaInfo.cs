using UnityEngine;
using System.Collections;
using System;
using UnityEngine.UI;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using System.Runtime.InteropServices;
using LitJson;
using System.Collections.Generic;

namespace dpn
{
    public class MediaInfo
    {
        public string channelLayout = null;
        public string videoCodec = null;
        public string audioCodec = null;
        public int videoHeight = -1;
        public int videoWidth = -1;
        public string videoPath = null;
        public bool isEncrypted = false;
        public string encryptKey = null;
        public string encryptKeyFilePath = null;
        public int encryptType = -1;
        public int sarNum = 1;
        public int sarDen = 1;
        public bool gotInfo = false;
        public TrackInfo[] tracks;
        public bool isMutiChannelLayout
        {
            get
            {
                if ((channelLayout != null)
                    && (channelLayout != DecoderCapbility.CHANNEL_LAYOUT_MONO)
                    && (channelLayout != DecoderCapbility.CHANNEL_LAYOUT_STEREO))
                {
                    return true;
                }
                return false;
            }
        }
        public const int DPVR_ENCRYPT_V1 = 1;
        public const int DPVR_ENCRYPT_V2 = 2;

        public MediaInfo(string videoPath)
        {
            this.videoPath = videoPath;
        }

        public MediaInfo(string videoPath, IjkPlayerProxy player)
        {
            this.videoPath = videoPath;
            SetMediaInfo(player);
        }

        public void SetMediaInfo(IjkPlayerProxy player)
        {
            if (player.getCurrentDecoder() != IjkPlayerProxy.DECODER_MEDIACODEC)
            {
                return;
            }

            channelLayout = player.getChannelLayout();
            videoCodec = player.getVideoCodecName();
            audioCodec = player.getAudioCodecName();
            videoHeight = player.getVideoHeight();
            videoWidth = player.getVideoWidth();
            sarDen = player.getVideoSarDen();
            sarNum = player.getVideoSarNum();
            sarDen = sarDen == 0 ? 1 : sarDen;
            sarNum = sarNum == 0 ? 1 : sarNum;
            SetStreamInfo(player);
            gotInfo = true;

            DumpMediaInfo();
        }

        private void SetStreamInfo(IjkPlayerProxy player)
        {
            if (player.getCurrentDecoder() != IjkPlayerProxy.DECODER_MEDIACODEC)
            {
                return;
            }
            JsonData trackInfo = JsonMapper.ToObject(player.getTrackInfoJson());
            if (trackInfo != null)
            {
                this.tracks = new TrackInfo[trackInfo.Count];
                foreach (JsonData infoJson in trackInfo)
                {
                    TrackInfo track = new TrackInfo(infoJson);
                    if (track.index >= 0)
                    {
                        track.DumpInfo();
                        this.tracks[track.index] = track;
                    }
                }
            }
        }

        public void DumpMediaInfo()
        {
            Debug.Log("Dumping mediaInfo\n video codec :" + videoCodec + "\naudio codec:" + audioCodec + "\nsize:" + videoWidth + "  x " + videoHeight +
            "\n channel layout:" + channelLayout + "\n sar:" + sarDen + "/" + sarNum);
        }

        public static bool IsOnlineVideo(string url)
        {
            /* es file browser file start with local http server */
            if (!IsLANSharingFile(url) &&
                (url.StartsWith("http") || url.StartsWith("rtmp") || url.StartsWith("rtsp")))
            {
                return true;
            }
            return false;
        }

        public static bool IsLANSharingFile(string filePath)
        {
            return filePath.StartsWith("http://127.0.0.1") || filePath.StartsWith("http://localhost");
        }

        public static string GetSerialNoRaw()
        {
            if (IjkPlayer.Instance._player == null)
                return "";
            else return IjkPlayer.Instance._player.getSerialNo();
        }
    }
}
