using UnityEngine;
using System.Collections;
using System;
using UnityEngine.UI;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using System.Runtime.InteropServices;

namespace dpn
{
    public class IjkPlayer : MonoBehaviour
    {
        private static IjkPlayer instance;
        public static IjkPlayer Instance
        {
            get
            {
                if (instance == null)
                    instance = FindObjectOfType<IjkPlayer>();
                return instance;
            }
        }
        [HideInInspector]
        public bool ShouldPause = false;

        public static bool EnableYuvOpt = true;

        private bool firstStart = true;

        public MediaInfo mediaInfo = null;
        
        private CodecSelector _codecSelector = null;

        [Serializable]
        public class VideoOutput
        {
            public GameObject[] _vOuts;
            private Texture[] targetTexture;
            private CanvasRenderer[] canvasRenderers;

            public Action<Texture>[] setTextureActions
            {
                private set;
                get;
            }

            public GameObject[] VOuts
            {
                get
                {
                    return _vOuts;
                }
                set
                {
                    _vOuts = value;
                    targetTexture = new Texture[_vOuts.Length];
                    canvasRenderers = new CanvasRenderer[_vOuts.Length];
                    setTextureActions = new Action<Texture>[VOuts.Length];
                    bool renderViaTexture = false;

                    for (int index = 0; index != VOuts.Length; ++index)
                    {
                        GameObject obj = VOuts[index];
                        if (obj == null)
                            continue;
                        if (obj.GetComponent<CanvasRenderer>() != null)
                        {
                            if (EnableYuvOpt)
                            {
                                targetTexture[index] = obj.GetComponent<RawImage>().material.mainTexture;
                                renderViaTexture = true;
                            }
                            else
                                canvasRenderers[index] = obj.GetComponent<CanvasRenderer>();
                        }
                        else if (obj.GetComponent<MeshRenderer>() != null)
                        {
                            targetTexture[index] = obj.GetComponent<MeshRenderer>().material.mainTexture;
                            renderViaTexture = true;
                        }

                        if (renderViaTexture)
                        {
                            Texture target = targetTexture[index];
                            setTextureActions[index] = ((Texture texture) => { target = texture; });
                        }
                        else
                        {
                            CanvasRenderer target = canvasRenderers[index];
                            setTextureActions[index] = ((Texture texture) => { target.SetTexture(texture); });
                        }

                    }
                }
            }

            public void SetTexture(Texture2D tex)
            {
                for (int index = 0; index != setTextureActions.Length; ++index)
                {
                    setTextureActions[index](tex);
                }
            }
        }

        public event Action OnFrameAvailableCallback;
        public event Action OnPlayCompleteCallback;
        public event Action OnSeekCompleteCallback;
        public event Action OnPauseCallback;
        public event Action OnResumeCallback;
        public event Action OnStopCallback;
        public event Action<bool> OnPreparedCallback;//bool->whether first start
        public event Action<int, int> OnMediaPlayerErrorCallback;//int->errorId int->ExtraInfo
        public event Action<int, int> OnVideoChangedCallback;//int->width int->height

        private void OnComplete(AndroidJavaObject mp)
        {
            _texture = null;
            WaitingForFirstFrame = true;
            if (AutoLoop)
            {
                LoopVideo();
                return;
            }
            _startSeekComplete = false;
            if (OnPlayCompleteCallback != null && !AutoLoop)
                OnPlayCompleteCallback();
        }

        public void LoopVideo()
        {
            StartCoroutine(_Loop());
        }

        private IEnumerator _Loop()
        {
            yield return null;
            Play(MediaUrl, 0);
        }

        #region handlers

        public bool WaitingForFirstFrame = true;
        //    int lastFrame = 0;
        void OnFrameAvailable()
        {
            /*
            Debug.Log("Speed check : cached duration " + _player.getVideoCachedDuration());
            int tNow = System.Environment.TickCount;
            int timeSkip = tNow - lastFrame;
            lastFrame = tNow;
            Debug.Log("Speed check: time skip " + timeSkip);
            */
            if (WaitingForFirstFrame)
            {
                SetVideoSVMatrix();
                WaitingForFirstFrame = false;
            }

            if (OnFrameAvailableCallback != null)
                OnFrameAvailableCallback();
            else
                Debug.Log("IjkPlayer OnFrameAvailable");
        }

        void OnPrepared(AndroidJavaObject mp)
        {
            Debug.Log("Player on prepared, using : " + _player.getCurrentDecoder() + "  vc : "
                + _player.getVideoCodecName() + "  ac:" + _player.getAudioCodecName());
            Debug.Log("Player channel layout :" + _player.getChannelLayout());
            //to ensure get correct mediaInfo, only works in mediacodec.
            perpareSyncFlag++;
            mediaInfo.SetMediaInfo(_player);
            _bestDecoder = GetBestDecoder();
            if (_bestDecoder < 0)
            {
                noticfyPlayFailed();
                return;
            }

            if (_bestDecoder != _player.getCurrentDecoder())
            {
                Play(MediaUrl, StartTime, _bestDecoder);
                return;
            }

            _isPrepared = true;
            _startSeekComplete = true;

            if (StartTime != 0)
                _startSeekComplete = false;
            StartCoroutine(_SetPlay());
        }

        IEnumerator _SetPlay()
        {
            yield return null;
            if (_player != null && !ShouldPause)
                _player.start();

            if (StartTime != 0)
                Seek(StartTime);

            this._duration = (float)_player.getDuration() / 1000.0f;
            SourceVideoRatio = (float)(mediaInfo.videoHeight * 1.0f / mediaInfo.sarNum) / (mediaInfo.videoWidth * 1.0f / mediaInfo.sarDen);
            if (OnPreparedCallback != null)
            {
                Debug.Log("Call Onprepared event");
                OnPreparedCallback(firstStart);
                firstStart = false;
            }
        }

        IEnumerator _ReloadIn2S()
        {
            //  yield return new WaitForSecondsRealtime(2);
            Debug.Log("Try to reload video");
            Play(MediaUrl, StartTime);
            yield return null;

        }

        void OnBufferingUpdate(AndroidJavaObject mp, int percent)
        {
            _bufferedPercentage = percent;
        }
        void OnSeekComplete(AndroidJavaObject mp)
        {
            _startSeekComplete = true;
            _isSeeking = false;
            if (OnSeekCompleteCallback != null)
                OnSeekCompleteCallback();
            Debug.Log("IjkPlayer OnSeekComplete");
        }
        void OnVideoSizeChanged(AndroidJavaObject mp, int width, int height, int sar_num, int sar_den)
        {
            float ratio = (float)(VOutHeight / mediaInfo.sarNum) / (VOutWidth / mediaInfo.sarDen);
            if (VOutWidth == width && VOutHeight == height && SourceVideoRatio == ratio)
                return;
            Debug.Log("Video size is " + width + " height is " + height + " sar_den is " + mediaInfo.sarDen + " sar_num is " + mediaInfo.sarNum);
            VOutWidth = width;
            VOutHeight = height;
            SourceVideoRatio = ratio;

            Debug.Log("Source Video Ratio is " + SourceVideoRatio);
            if (OnVideoChangedCallback != null)
            {
                OnVideoChangedCallback(VOutWidth, VOutHeight);
            }
        }

        private int _tryReloadTimes = 0;
        private const int MAX_TRY_RELOAD_TIME = 4;

        bool OnError(AndroidJavaObject mp, int what, int extra)
        {
            string error = "";

            switch (what)
            {
                case MediaError.MEDIA_ERROR_UNKNOWN:
                    error = "MEDIA_ERROR_UNKNOWN";
                    break;
                case MediaError.MEDIA_ERROR_SERVER_DIED:
                    error = "MEDIA_ERROR_SERVER_DIED";
                    break;
                case MediaError.MEDIA_ERROR_NOT_VALID_FOR_PROGRESSIVE_PLAYBACK:
                    error = "MEDIA_ERROR_NOT_VALID_FOR_PROGRESSIVE_PLAYBACK";
                    break;
                case MediaError.MEDIA_ERROR_IO:
                    error = "MEDIA_ERROR_IO";
                    break;
                case MediaError.MEDIA_ERROR_MALFORMED:
                    error = "MEDIA_ERROR_MALFORMED";
                    break;
                case MediaError.MEDIA_ERROR_UNSUPPORTED:
                    error = "MEDIA_ERROR_UNSUPPORTED";
                    break;
                case MediaError.MEDIA_ERROR_TIMED_OUT:
                    error = "MEDIA_ERROR_TIMED_OUT";
                    break;
                case MediaError.MEDIA_ERROR_IJK_PLAYER:
                    error = "MEDIA_ERROR_IJK_PLAYER";

                    // 硬解码错误
                    if (extra == -1010)
                    {
                        error += ",MEDIACODEC_FAIL";
                    }
                    break;

                default:
                    error = "MEDIA_ERROR_UNKNOWN";
                    break;
            }


            Debug.Log("IjkPlayer error:[" + what + "," + extra + "] " + error);
            _player.stop();
            /* always try to reload */
            if (_tryReloadTimes <= MAX_TRY_RELOAD_TIME)
            {
                StartCoroutine(_ReloadIn2S());
                _tryReloadTimes++;
            }
            else
            {
                what = MediaError.MEDIA_ERROR_TOO_MANY_RELOADTIMES;
                _tryReloadTimes = 0;
            }

            if (OnMediaPlayerErrorCallback != null)
            {
                OnMediaPlayerErrorCallback(what, extra);
            }

            /*
            PlayerController.Instance.ExitPlayerImmediately();
            */
            return true;
        }
        bool OnInfo(AndroidJavaObject mp, int what, int extra)
        {
            string info = "";

            switch (what)
            {
                case IjkPlayerProxy.MEDIA_INFO_UNKNOWN:
                    info = "MEDIA_INFO_UNKNOWN";
                    break;
                case IjkPlayerProxy.MEDIA_INFO_STARTED_AS_NEXT:
                    info = "MEDIA_INFO_STARTED_AS_NEXT";
                    break;
                case IjkPlayerProxy.MEDIA_INFO_VIDEO_RENDERING_START:
                    info = "MEDIA_INFO_VIDEO_RENDERING_START";
                    break;
                case IjkPlayerProxy.MEDIA_INFO_VIDEO_TRACK_LAGGING:
                    info = "MEDIA_INFO_VIDEO_TRACK_LAGGING";
                    break;
                case IjkPlayerProxy.MEDIA_INFO_BUFFERING_START:
                    info = "MEDIA_INFO_BUFFERING_START";
                    break;
                case IjkPlayerProxy.MEDIA_INFO_BUFFERING_END:
                    info = "MEDIA_INFO_BUFFERING_END";
                    break;
                case IjkPlayerProxy.MEDIA_INFO_NETWORK_BANDWIDTH:
                    info = "MEDIA_INFO_NETWORK_BANDWIDTH";
                    break;
                case IjkPlayerProxy.MEDIA_INFO_BAD_INTERLEAVING:
                    info = "MEDIA_INFO_BAD_INTERLEAVING";
                    break;
                case IjkPlayerProxy.MEDIA_INFO_NOT_SEEKABLE:
                    info = "MEDIA_INFO_NOT_SEEKABLE";
                    break;
                case IjkPlayerProxy.MEDIA_INFO_METADATA_UPDATE:
                    info = "MEDIA_INFO_METADATA_UPDATE";
                    break;
                case IjkPlayerProxy.MEDIA_INFO_TIMED_TEXT_ERROR:
                    info = "MEDIA_INFO_TIMED_TEXT_ERROR";
                    break;
                case IjkPlayerProxy.MEDIA_INFO_UNSUPPORTED_SUBTITLE:
                    info = "MEDIA_INFO_UNSUPPORTED_SUBTITLE";
                    break;
                case IjkPlayerProxy.MEDIA_INFO_SUBTITLE_TIMED_OUT:
                    info = "MEDIA_INFO_SUBTITLE_TIMED_OUT";
                    break;

                case IjkPlayerProxy.MEDIA_INFO_VIDEO_ROTATION_CHANGED:
                    info = "MEDIA_INFO_VIDEO_ROTATION_CHANGED";
                    break;
                case IjkPlayerProxy.MEDIA_INFO_AUDIO_RENDERING_START:
                    info = "MEDIA_INFO_AUDIO_RENDERING_START";
                    break;
                default:
                    info = "MEDIA_INFO_UNKNOWN";
                    break;
            }

            //Debug.Log("IjkPlayer info:[" + what + "," + extra + "] " + info);
            return true;
        }
        #endregion

        public float StartTime = 0;
        public int FrameQueue = 2;
        public bool AutoLoop = false;


        private int VOutWidth = 128;
        private int VOutHeight = 128;

        public float SourceVideoRatio
        {
            get;
            private set;
        }
        private bool _startSeekComplete = false;
        public bool StartSeekComplete { get { return _startSeekComplete; } }
        private bool _isSeeking = false;
        public bool IsSeeking { get { return _isSeeking; } }


        public float Current
        {
            get
            {
                if (IsReady == false) { return 0.0f; }
                return (float)_player.getCurrentPosition() / 1000.0f;
            }
        }
        public bool IsPlaying
        {
            get
            {
                if (_player == null) { return false; }
                return _player.isPlaying();
            }
        }

        public string PlayingMediaUrl
        {
            get
            {
                return IsPlaying ? MediaUrl : "";
            }
        }

        private float _duration = 0.0f;
        public float Duration
        {
            get
            {
                return this._duration;
            }
        }

        public float Buffered
        {
            get
            {
                if (IsReady == false) { return 0.0f; }
                return (float)_bufferedPercentage / 100.0f * Duration;
            }
        }
        public bool IsReady
        {
            get { return _player != null && _isPrepared == true; }
        }

        private int _bufferedPercentage = 0;
        private bool _isPrepared = false;

        public bool AutoPlay = true;
        public string MediaUrl = "";


        Texture2D _texture;


        public IjkPlayerProxy _player = null;

        public VideoOutput VOut;
        string deviceMode = null;

        protected void Start()
        {
            _codecSelector = new P1ProCodecSelector();

            if (_player==null)
            {
                deviceMode = null;
            }
            else
            {
                deviceMode = _player.getDeviceMode();
            }
          
            if (deviceMode == "P1Pro")
                _codecSelector = new P1ProCodecSelector();
            else
                _codecSelector = new P1CodecSelector();

            if (AutoPlay)
                Play(MediaUrl, StartTime);
        }


        void SetListeners(IjkPlayerProxy player)
        {
            player.resetListeners();
            player.setOnFrameAvailable(new IjkPlayerProxy.OnVideoFrameAvailableListener(OnFrameAvailable));
            player.setOnPreparedListener(new IjkPlayerProxy.OnPreparedListener(OnPrepared));
            player.setOnCompletionListener(new IjkPlayerProxy.OnCompletionListener(OnComplete));
            player.setOnBufferingUpdateListener(new IjkPlayerProxy.OnBufferingUpdateListener(OnBufferingUpdate));
            player.setOnSeekCompleteListener(new IjkPlayerProxy.OnSeekCompleteListener(OnSeekComplete));
            player.setOnVideoSizeChangedListener(new IjkPlayerProxy.OnVideoSizeChangedListener(OnVideoSizeChanged));
            player.setOnErrorListener(new IjkPlayerProxy.OnErrorListener(OnError));
            player.setOnInfoListener(new IjkPlayerProxy.OnInfoListener(OnInfo));
        }

        void Update()
        {
            if (VOut != null && IsReady && _texture != null && VOut.setTextureActions != null)
            {
                // external texture don't need reseize, its size is the same as video
                if (!EnableYuvOpt)
                {
                    if (_texture.width != VOutWidth || _texture.height != VOutHeight)
                    {
                        _texture.Resize(VOutWidth, VOutHeight, TextureFormat.RGBA32, false);
                        _texture.Apply(false, false);
                        _player.setTargetTexture(_texture.GetNativeTexturePtr(), 0, 0, VOutWidth, VOutHeight);
                    }
                }
                _player.updateTarget(_startSeekComplete);
                if (_startSeekComplete)
                {
                    VOut.SetTexture(_texture);
                }
            }
        }

        private VP1PlayerOptions options = null;

        public void Play(string url, float seek = 0, int decoder = IjkPlayerProxy.DECODER_UNKNOWN, VP1PlayerOptions options = null)
        {
            Debug.Log("Ijkplayer now playing: " + url + " seek position: " + seek);

            if (_player != null)
                _player.reset();
            Stop();
            WaitingForFirstFrame = true;



            if (MediaUrl != url)
            {

                mediaInfo = new MediaInfo(MediaUrl);
                ResetMediaInfo();
            }

            MediaUrl = url;

            Debug.Log("Ijkplayer now playing: " + MediaUrl);

            if (_bestDecoder == IjkPlayerProxy.DECODER_UNKNOWN)
            {
                _bestDecoder = (decoder == IjkPlayerProxy.DECODER_UNKNOWN ? IjkPlayerProxy.DECODER_MEDIACODEC : decoder);
            }

            /* load option */
            this.options = options;
            LoadOption(options);
            if (mediaInfo.isEncrypted && _bestDecoder == IjkPlayerProxy.DECODER_ANDROID_MEDIAPLAYER)
            {
                _bestDecoder = IjkPlayerProxy.DECODER_MEDIACODEC;
            }

            Debug.Log("Current best decoder = " + _bestDecoder);
            _player = new IjkPlayerProxy(EnableYuvOpt, _bestDecoder);
            if (mediaInfo.isEncrypted)
            {
                if(!string.IsNullOrEmpty(mediaInfo.encryptKey)){
                    _player.setOption(IjkPlayerProxy.OPT_CATEGORY_PLAYER, "dpvr_encrypt",mediaInfo.encryptKey);
                }
                else if (!string.IsNullOrEmpty(mediaInfo.encryptKeyFilePath)){
                    _player.setOption(IjkPlayerProxy.OPT_CATEGORY_PLAYER,"key-file-path",mediaInfo.encryptKeyFilePath);
                }
            }

            SetListeners(_player);

            if (_texture == null)
            {
                Debug.Log("create texture : width  " + VOutWidth + "enable yuv :" + EnableYuvOpt);
                if (EnableYuvOpt)
                {
                    _texture = Texture2D.CreateExternalTexture(VOutWidth, VOutHeight, TextureFormat.RGBA32, false, false, (IntPtr)_player.getVideoTextureId());
                }
                else
                {
                    _texture = new Texture2D(VOutWidth, VOutHeight, TextureFormat.RGBA32, false);
                }               
                _texture.filterMode = FilterMode.Bilinear;
            }

            if (!EnableYuvOpt)
            {
                _player.setTargetTexture(_texture.GetNativeTexturePtr(), 0, 0, _texture.width, _texture.height);
            }

            StartTime = seek;

            if (StartTime < 0)
                StartTime = 0;

            _player.setDataSource(MediaUrl);
            if (IsPlayingOnline())
            {
                perpareSyncFlag++;
                StartCoroutine(StartPrepareSyncCountDown(perpareSyncFlag));
            };

            _player.prepareAsync();
        }

        /* flag to ensure when coroutine is finshed , still wait for the prepareSycn when it started*/
        private int perpareSyncFlag = 0;
        IEnumerator StartPrepareSyncCountDown(int flag)
        {
            yield return new WaitForSeconds(15.0f);
            if (flag == perpareSyncFlag)
            {
                noticfyPlayFailed();
            }
        }

        public void Stop()
        {
            StopAllCoroutines();
            perpareSyncFlag++;

            if (_player == null)
            {
                return;
            }
            _player.resetListeners();
            _player.release();
            _player = null;
            _isPrepared = false;
            if (OnStopCallback != null)
                OnStopCallback();
        }

        public void Seek(float sec)
        {
            if (IsReady == false)
                return;
            _isSeeking = true;
            sec = sec < 0 ? 0 : sec;

            _player.seekTo((int)(sec * 1000));
        }
        public void Pause()
        {
            if (IsReady == false)
            {
                return;
            }
            if (OnPauseCallback != null)
                OnPauseCallback();
            _player.pause();
        }
        public void Resume()
        {
            if (IsReady == false)
            {
                return;
            }
            if (OnResumeCallback != null)
                OnResumeCallback();
            _player.start();
        }

        void OnDestroy()
        {
            Stop();
            _texture = null;
        }

        public void SetOption(int category, string name, string value)
        {
            if (_player == null)
            {
                return;
            }
            _player.setOption(category, name, value);

        }
        public void SetOption(int category, string name, long value)
        {
            if (_player == null)
            {
                return;
            }
            _player.setOption(category, name, value);
        }

        public void SetVideoSVMatrix()
        {
            if (!EnableYuvOpt || _player == null)
                return;

            float[] matrix = _player.getVideoSVMatrix();
            Matrix4x4 matrix4X4 = new Matrix4x4();
            for (int index = 0; index != matrix.Length; ++index)
            {
                matrix4X4[index] = matrix[index];
            }
            foreach (var o in VOut.VOuts)
            {
                if (o == null)
                    continue;

                if (o.GetComponent<CanvasRenderer>() != null)
                {
                    o.GetComponent<RawImage>().material.SetMatrix("stMatrix", matrix4X4);
                }
                else if (o.GetComponent<MeshRenderer>() != null)
                {
                    o.GetComponent<MeshRenderer>().material.SetMatrix("stMatrix", matrix4X4);
                }
            }
        }

        private Boolean IsPlayingOnline()
        {
            if (MediaUrl != null && (MediaUrl.StartsWith("http") || MediaUrl.StartsWith("rtmp") || MediaUrl.StartsWith("rtsp")))
            {
                return true;
            }
            return false;
        }

        private bool CheckDecoderSupport(int decoder, string videoCodec = null, string audioCodec = null)
        {
            return DecoderCapbility.Instance().CheckIsEnableToDecode(decoder, videoCodec) && DecoderCapbility.Instance().CheckIsEnableToDecode(decoder, audioCodec);
        }

        private void GetMediaInfo()
        {
            MediaInfo mediaInfo = new MediaInfo(MediaUrl, _player);
        }

        private int GetBestDecoder()
        {
            int result = _codecSelector.GetBestDecoder(mediaInfo);
            return result == IjkPlayerProxy.DECODER_UNKNOWN ? _player.getCurrentDecoder() : result;
        }

        public void SetSyncWaitTime(int time)
        {
            if (_player == null)
                return;
            _player.setSyncWaitTime(time);
        }

        private int _bestDecoder = IjkPlayerProxy.DECODER_UNKNOWN;
        private void ResetMediaInfo()
        {
            _bestDecoder = IjkPlayerProxy.DECODER_UNKNOWN;
            _duration = 0.0f;
        }

        public void SetAudioTrack(int index)
        {
            _player.selectTrack(index);
        }

        private bool LoadOption(VP1PlayerOptions options)
        {
            bool ret = false;
            if (options != null)
            {
                if (options.GetBool(VP1PlayerOptions.PLAYER_OPTIONS.BOOL_IS_DPVR_VIDEO))
                {
                    _bestDecoder = _codecSelector.GetDefaultDPVRDecoder();
                    ret = true;
                }
                string encryptKeyPath = options.GetString(VP1PlayerOptions.PLAYER_OPTIONS.STRING_ENCRYPT_KEY_FILE_PATH);
                string key = options.GetString(VP1PlayerOptions.PLAYER_OPTIONS.STRING_ENCRYPT_KEY);
                if(!string.IsNullOrEmpty(key)){
                    mediaInfo.isEncrypted = true;
                    mediaInfo.encryptKey = key;
                }
                else if(!string.IsNullOrEmpty(encryptKeyPath)){
                    mediaInfo.isEncrypted = true;
                    mediaInfo.encryptKeyFilePath = encryptKeyPath;
                }
            }
            ret = true;
            return ret;
        }

        private void noticfyPlayFailed()
        {
            if (_player != null)
            {
                _player.resetListeners();
                _player.release();
                _player = null;
                _isPrepared = false;
                if (OnMediaPlayerErrorCallback != null)
                {
                    OnMediaPlayerErrorCallback(MediaError.MEDIA_ERROR_UNSUPPORTED, 0);
                }
            }
        }
    }
}

