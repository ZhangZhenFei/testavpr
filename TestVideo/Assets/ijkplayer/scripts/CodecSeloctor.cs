using UnityEngine;
namespace dpn
{
    public interface CodecSelector
    {
        void InitDecoderCapbility();
        int GetDefaultDPVRDecoder();
        int GetBestDecoder(MediaInfo info);

    }

}
