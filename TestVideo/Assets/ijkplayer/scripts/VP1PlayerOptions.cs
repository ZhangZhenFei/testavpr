﻿using System;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using System.Text;

namespace dpn
{
    public class VP1PlayerOptions
    {
        public enum PLAYER_OPTIONS
        {
            BOOL_IS_DPVR_VIDEO,
            BOOL_IS_ENCRYPTED,
            STRING_ENCRYPT_KEY,
            STRING_ENCRYPT_KEY_FILE_PATH
        };

        private const string TAG = "VP1PlayerOption";
        Dictionary<string, string> settings;
        private const string KEYWORD_INT_VALUE = "INT_";
        private const string KEYWORD_BOOL_VALUE = "BOOL_";
        private const string KEYWORD_STRING_VALUE = "STRING_";
        private const string KEYWORD_TRUE = "true";
        private const string KEYWORD_FALSE = "false";

        public VP1PlayerOptions()
        {
            this.settings = new Dictionary<string, string>();
            SetOption(PLAYER_OPTIONS.BOOL_IS_DPVR_VIDEO, false);
            SetOption(PLAYER_OPTIONS.BOOL_IS_ENCRYPTED, false);
            SetOption(PLAYER_OPTIONS.STRING_ENCRYPT_KEY,null);
            SetOption(PLAYER_OPTIONS.STRING_ENCRYPT_KEY_FILE_PATH,null);
        }

        private void removeKey(PLAYER_OPTIONS option)
        {
            if (this.settings.ContainsKey(option.GetType().ToString()))
            {
                this.settings.Remove(option.GetType().ToString() + option);
            }
        }

        public void SetOption(PLAYER_OPTIONS option, String value)
        {
            if (value != null)
            {
                removeKey(option);
                this.settings.Add(option.GetType().ToString() + option, KEYWORD_STRING_VALUE + value);
            }
        }

        public void SetOption(PLAYER_OPTIONS option, int value)
        {
            removeKey(option);
            this.settings.Add(option.GetType().ToString() + option, KEYWORD_INT_VALUE + value);
        }

        public void SetOption(PLAYER_OPTIONS option, bool isTrue)
        {
            removeKey(option);
            string key = option.GetType().ToString() + option;
            string value = isTrue ? KEYWORD_BOOL_VALUE + KEYWORD_TRUE : KEYWORD_BOOL_VALUE + KEYWORD_FALSE;
            this.settings.Add(key, value);
        }

        private string GetOption(PLAYER_OPTIONS option)
        {
            string key = option.GetType().ToString() + option;
            if (this.settings.ContainsKey(key))
            {
                return this.settings[key];
            }
            return "";
        }

        public int GetInt(PLAYER_OPTIONS option)
        {
            string rawValue = GetOption(option) ;
            if (rawValue != null && rawValue.StartsWith(KEYWORD_INT_VALUE))
            {
                string value = rawValue.Substring(KEYWORD_INT_VALUE.Length, rawValue.Length - 1);
                return int.Parse(value);
            }
            return 0;
        }

        public string GetString(PLAYER_OPTIONS option)
        {
            string rawValue = GetOption(option);
            if (rawValue != null && rawValue.StartsWith(KEYWORD_STRING_VALUE))
            {
                return rawValue.Substring(KEYWORD_STRING_VALUE.Length, rawValue.Length - KEYWORD_STRING_VALUE.Length);
            }
            Debug.Log(TAG + "GetString error, option" + option.GetType().ToString() + " value : " + rawValue);
            return null;
        }

        public bool GetBool(PLAYER_OPTIONS option)
        {
            string rawValue = GetOption(option);
            if (rawValue != null && rawValue.StartsWith(KEYWORD_BOOL_VALUE))
            {
                return rawValue.Contains(KEYWORD_TRUE) ? true : false;
            }
            Debug.Log(TAG + "GetBool error, option" + option.GetType().ToString() + " value : " + rawValue);
            return false;
        }
    }
}
