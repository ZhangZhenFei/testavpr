using UnityEngine;
using System.Collections;
using UnityEngine.Profiling;

namespace dpn
{

    public class IjkPlayerProxy
    {
        public const int MEDIA_INFO_UNKNOWN = 1;
        public const int MEDIA_INFO_STARTED_AS_NEXT = 2;
        public const int MEDIA_INFO_VIDEO_RENDERING_START = 3;
        public const int MEDIA_INFO_VIDEO_TRACK_LAGGING = 700;
        public const int MEDIA_INFO_BUFFERING_START = 701;
        public const int MEDIA_INFO_BUFFERING_END = 702;
        public const int MEDIA_INFO_NETWORK_BANDWIDTH = 703;
        public const int MEDIA_INFO_BAD_INTERLEAVING = 800;
        public const int MEDIA_INFO_NOT_SEEKABLE = 801;
        public const int MEDIA_INFO_METADATA_UPDATE = 802;
        public const int MEDIA_INFO_TIMED_TEXT_ERROR = 900;
        public const int MEDIA_INFO_UNSUPPORTED_SUBTITLE = 901;
        public const int MEDIA_INFO_SUBTITLE_TIMED_OUT = 902;

        public const int MEDIA_INFO_VIDEO_ROTATION_CHANGED = 10001;
        public const int MEDIA_INFO_AUDIO_RENDERING_START = 10002;

        public const int DECODER_UNKNOWN = 2000;
        public const int DECODER_FFMPEG = 2001;
        public const int DECODER_MEDIACODEC = 2002;
        public const int DECODER_ANDROID_MEDIAPLAYER = 2003;


        #region listeners
        public delegate void OnVideoFrameAvailable();
        public class OnVideoFrameAvailableListener : AndroidJavaProxy
        {
            OnVideoFrameAvailable _handler;
            public OnVideoFrameAvailableListener(OnVideoFrameAvailable handler) : base("tv.danmaku.ijk.media.player.IjkUnityProxy$OnVideoFrameAvailable")
            {
                _handler = handler;
            }
            void OnFrameAvailable()
            {
                _handler();
            }
        }
        public delegate void OnPreparedHandler(AndroidJavaObject mp);

        public class OnPreparedListener : AndroidJavaProxy
        {
            OnPreparedHandler _handler;
            public OnPreparedListener(OnPreparedHandler handler) : base("tv.danmaku.ijk.media.player.IMediaPlayer$OnPreparedListener")
            {
                _handler = handler;
            }
            void onPrepared(AndroidJavaObject mp)
            {
                _handler(mp);
            }
        }

        public delegate void OnCompleteHandler(AndroidJavaObject mp);

        public class OnCompletionListener : AndroidJavaProxy
        {
            OnCompleteHandler _handler;
            public OnCompletionListener(OnCompleteHandler handler) : base("tv.danmaku.ijk.media.player.IMediaPlayer$OnCompletionListener")
            {
                _handler = handler;
            }
            void onCompletion(AndroidJavaObject mp)
            {
                _handler(mp);
            }
        }

        public delegate void OnBufferingUpdateHandler(AndroidJavaObject mp, int percent);

        public class OnBufferingUpdateListener : AndroidJavaProxy
        {
            OnBufferingUpdateHandler _handler;
            public OnBufferingUpdateListener(OnBufferingUpdateHandler handler) : base("tv.danmaku.ijk.media.player.IMediaPlayer$OnBufferingUpdateListener")
            {
                _handler = handler;
            }
            void onBufferingUpdate(AndroidJavaObject mp, int percent)
            {
                _handler(mp, percent);
            }
        }

        public delegate void OnSeekCompleteHandler(AndroidJavaObject mp);
        public class OnSeekCompleteListener : AndroidJavaProxy
        {
            OnSeekCompleteHandler _handler;
            public OnSeekCompleteListener(OnSeekCompleteHandler handler) : base("tv.danmaku.ijk.media.player.IMediaPlayer$OnSeekCompleteListener")
            {
                _handler = handler;
            }
            void onSeekComplete(AndroidJavaObject mp)
            {
                _handler(mp);
            }
        }

        public delegate void OnVideoSizeChangedHandler(AndroidJavaObject mp, int width, int height, int sar_num, int sar_den);
        public class OnVideoSizeChangedListener : AndroidJavaProxy
        {
            OnVideoSizeChangedHandler _handler;
            public OnVideoSizeChangedListener(OnVideoSizeChangedHandler handler) : base("tv.danmaku.ijk.media.player.IMediaPlayer$OnVideoSizeChangedListener")
            {
                _handler = handler;
            }
            void onVideoSizeChanged(AndroidJavaObject mp, int width, int height, int sar_num, int sar_den)
            {
                _handler(mp, width, height, sar_num, sar_den);
            }
        }

        public delegate bool OnErrorHandler(AndroidJavaObject mp, int what, int extra);

        public class OnErrorListener : AndroidJavaProxy
        {
            OnErrorHandler _handler;
            public OnErrorListener(OnErrorHandler handler) : base("tv.danmaku.ijk.media.player.IMediaPlayer$OnErrorListener")
            {
                _handler = handler;
            }
            bool onError(AndroidJavaObject mp, int what, int extra)
            {
                return _handler(mp, what, extra);
            }
        }

        public delegate bool OnInfoHandler(AndroidJavaObject mp, int what, int extra);
        public class OnInfoListener : AndroidJavaProxy
        {
            OnInfoHandler _handler;
            public OnInfoListener(OnInfoHandler handler) : base("tv.danmaku.ijk.media.player.IMediaPlayer$OnInfoListener")
            {
                _handler = handler;
            }
            bool onInfo(AndroidJavaObject mp, int what, int extra)
            {
                return _handler(mp, what, extra);
            }
        }
        #endregion

        public static int OPT_CATEGORY_FORMAT = 1;
        public static int OPT_CATEGORY_CODEC = 2;
        public static int OPT_CATEGORY_SWS = 3;
        public static int OPT_CATEGORY_PLAYER = 4;

        AndroidJavaObject _mp;

        class FUpdateTarget
        {
            AndroidJavaObject _this;
            System.IntPtr _method;
            System.IntPtr _object;
            jvalue[] _params = new jvalue[1];

            public FUpdateTarget(AndroidJavaObject obj)
            {
                _this = obj;

                _method = AndroidJNIHelper.GetMethodID(_this.GetRawClass(), "updateTarget");
                _object = _this.GetRawObject();
            }
            public void Call(bool display)
            {
                Profiler.BeginSample("CallVoidMethod");
                _params[0].z = display;
                AndroidJNI.CallVoidMethod(_object, _method, _params);
                Profiler.EndSample();
            }
        }

        FUpdateTarget _updateTarget;

        public IjkPlayerProxy(bool EnableYuvOpt, int decoder = DECODER_ANDROID_MEDIAPLAYER)
        {
            _mp = new AndroidJavaObject("tv.danmaku.ijk.media.player.IjkUnityProxy", EnableYuvOpt, decoder);

            _updateTarget = new FUpdateTarget(_mp);
        }

        public void start()
        {
            _mp.Call("start");
        }
        public void stop()
        {
            _mp.Call("stop");
        }
        public void pause()
        {
            _mp.Call("pause");
        }
        public int getVideoWidth()
        {
            return _mp.Call<int>("getVideoWidth");
        }
        public int getVideoHeight()
        {
            return _mp.Call<int>("getVideoHeight");
        }
        public bool isPlaying()
        {
            return _mp.Call<bool>("isPlaying");
        }
        public void seekTo(int msec)
        {
            _mp.Call("seekTo", msec);
        }
        public long getCurrentPosition()
        {
            return _mp.Call<long>("getCurrentPosition");
        }
        public long getDuration()
        {
            return _mp.Call<long>("getDuration");
        }
        public void setDataSource(string url)
        {
            _mp.Call("setDataSource", url);
        }
        public void release()
        {
            _mp.Call("release");
        }
        public void reset()
        {
            _mp.Call("reset");
        }

        public void setOption(int category, string name, string value)
        {
            _mp.Call("setOption", category, name, value);
        }
        public void setOption(int category, string name, long value)
        {
            _mp.Call("setOption", category, name, value);
        }

        public void setOnPreparedListener(AndroidJavaProxy listener)
        {
            _mp.Call("setOnPreparedListener", listener);
        }
        public void setOnCompletionListener(AndroidJavaProxy listener)
        {
            _mp.Call("setOnCompletionListener", listener);
        }
        public void setOnBufferingUpdateListener(AndroidJavaProxy listener)
        {
            _mp.Call("setOnBufferingUpdateListener", listener);
        }
        public void setOnSeekCompleteListener(AndroidJavaProxy listener)
        {
            _mp.Call("setOnSeekCompleteListener", listener);
        }
        public void setOnVideoSizeChangedListener(AndroidJavaProxy listener)
        {
            _mp.Call("setOnVideoSizeChangedListener", listener);
        }
        public void setOnErrorListener(AndroidJavaProxy listener)
        {
            _mp.Call("setOnErrorListener", listener);
        }
        public void setOnInfoListener(AndroidJavaProxy listener)
        {
            _mp.Call("setOnInfoListener", listener);
        }
        public void setOnFrameAvailable(AndroidJavaProxy listener)
        {
            _mp.Call("setOnFrameAvailable", listener);
        }
        public void resetListeners()
        {
            _mp.Call("resetListeners");
        }

        public void prepareAsync()
        {
            _mp.Call("prepareAsync");
        }
        public void setTargetTexture(System.IntPtr tex, int x, int y, int w, int h)
        {
            _mp.Call("setTargetTexture", tex.ToInt32(), x, y, w, h);
        }
        public void updateTarget(bool display = true)
        {
            _updateTarget.Call(display);
        }

        public long getVideoCachedDuration()
        {
            return _mp.Call<long>("getVideoCachedDuration");
        }

        public long getAudioCachedDuration()
        {
            return _mp.Call<long>("getAudioCachedDuration");
        }

        public int getVideoTextureId()
        {
            return _mp.Call<int>("getVideoTextureId");
        }

        public float[] getVideoSVMatrix()
        {
            return _mp.Call<float[]>("getVideoSTMatrix");
        }

        public int setVideoDecoder(int decoder)
        {
            return _mp.Call<int>("setVideoDecoder", decoder);
        }

        public int getCurrentDecoder()
        {
            return _mp.Call<int>("getCurrentDecoder");
        }

        public string getVideoCodecName()
        {
            return _mp.Call<string>("getVideoCodecName");
        }

        public string getAudioCodecName()
        {
            return _mp.Call<string>("getAudioCodecName");
        }

        public void setSyncWaitTime(int time)
        {
            _mp.Call("SetSyncWaitTime", time);
        }

        public string getChannelLayout()
        {
            return _mp.Call<string>("getChannelLayout");
        }

        public int getVideoSarNum()
        {
            return _mp.Call<int>("getVideoSarNum");
        }

        public int getVideoSarDen()
        {
            return _mp.Call<int>("getVideoSarDen");
        }

        public string getTrackInfoJson()
        {
            return _mp.Call<string>("getTrackInfoJson");
        }

        public void selectTrack(int streamIndex)
        {
            _mp.Call("selectTrack", streamIndex);
        }

        public string getSerialNo()
        {
            AndroidJavaObject util = new AndroidJavaObject("tv.danmaku.ijk.media.player.DpnUtil");
            return util.CallStatic<string>("getSerialNo");
        }

        public string getDeviceMode()
        {
            AndroidJavaObject util = new AndroidJavaObject("tv.danmaku.ijk.media.player.DpnUtil");
            return util.CallStatic<string>("getDeviceModel");
        }
    }
}
