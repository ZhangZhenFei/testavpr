
using UnityEngine;
using System.Collections;
using System;
using UnityEngine.UI;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using System.Runtime.InteropServices;
using System.Collections.Generic;
using System.Linq;

namespace dpn
{
    public class P1CodecSelector : CodecSelector
    {

        public P1CodecSelector()
        {
            InitDecoderCapbility();
        }

        public void InitDecoderCapbility()
        {
            DecoderCapbility.Instance().AddToBlackList("video/mp4v-es_0_1", IjkPlayerProxy.DECODER_MEDIACODEC);
            DecoderCapbility.Instance().AddToBlackList("ac3", IjkPlayerProxy.DECODER_ANDROID_MEDIAPLAYER);
            DecoderCapbility.Instance().AddToBlackList("video/avc_578_52",IjkPlayerProxy.DECODER_ANDROID_MEDIAPLAYER);
            //AddToBlackList("video/avc_77_31", IjkPlayerProxy.DECODER_ANDROID_MEDIAPLAYER);
            //AddToBlackList("video/avc_77_31", IjkPlayerProxy.DECODER_MEDIACODEC);
            DecoderCapbility.Instance().AddToForceDecoder("video/x-vnd.on2.vp9_0_1", IjkPlayerProxy.DECODER_ANDROID_MEDIAPLAYER);
            DecoderCapbility.Instance().AddToForceDecoder("video/wmv1_0_1", IjkPlayerProxy.DECODER_FFMPEG);
            DecoderCapbility.Instance().AddToForceDecoder("video/wmv2_0_1", IjkPlayerProxy.DECODER_FFMPEG);

            DecoderCapbility.Instance().HWSupportCodec.Add("avc");
            DecoderCapbility.Instance().HWSupportCodec.Add("hevc");
            DecoderCapbility.Instance().HWSupportCodec.Add("aac");

            DecoderCapbility.Instance().HWUnsupportMuxer.Add("ts");

        }

        public int GetDefaultDPVRDecoder()
        {
            return IjkPlayerProxy.DECODER_MEDIACODEC;
        }

        private bool CheckDecoderSupport(int decoder, string videoCodec, string audioCodec)
        {
            return DecoderCapbility.Instance().CheckIsEnableToDecode(decoder, videoCodec) && DecoderCapbility.Instance().CheckIsEnableToDecode(decoder, audioCodec);
        }

        public int GetBestDecoder(MediaInfo info)
        {
            int bestDecoder = IjkPlayerProxy.DECODER_UNKNOWN;
            if (!info.gotInfo)
            {
                return bestDecoder;
            }

            string ac = info.audioCodec;
            string vc = info.videoCodec;
            Dictionary<int, int> scoreMap = new Dictionary<int, int>();
            scoreMap.Add(IjkPlayerProxy.DECODER_ANDROID_MEDIAPLAYER, 10);
            scoreMap.Add(IjkPlayerProxy.DECODER_MEDIACODEC, 5);
            scoreMap.Add(IjkPlayerProxy.DECODER_FFMPEG, 0);

            if (info.isEncrypted || info.isMutiChannelLayout ||
                !DecoderCapbility.Instance().CheckHWMutexSupport(info.videoPath))
            {
                scoreMap.Remove(IjkPlayerProxy.DECODER_ANDROID_MEDIAPLAYER);
            }

            List<int> unsupportCodecs = new List<int>();
            foreach (var item in scoreMap)
            {
                if (!CheckDecoderSupport(item.Key, vc, ac))
                {
                    unsupportCodecs.Add(item.Key);
                }
            }

            foreach(var key in unsupportCodecs)
            {
                scoreMap.Remove(key);
            }

            int forceDecoder = DecoderCapbility.Instance().CheckForceDecoder(vc, ac, info.videoPath, MediaInfo.IsOnlineVideo(info.videoPath));
            if (scoreMap.ContainsKey(forceDecoder))
            {
                bestDecoder = forceDecoder;
            }
            else if(scoreMap.Count > 0)
            {
                bestDecoder = scoreMap.First(q => q.Value == scoreMap.Values.Max()).Key;
            }
            return bestDecoder;
        }

    }

}
