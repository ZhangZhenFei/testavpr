Shader "DpnLauncher/YUVTexture"
{
	Properties
	{
		_MainTex("Texture", 2D) = "white" {}
	}
	SubShader
	{ // Unity chooses the subshader that fits the GPU best	

		ZTest Always

		Pass
		{ // some shaders require multiple passes
		GLSLPROGRAM // here begins the part in Unity's GLSL

		#ifdef VERTEX // here begins the vertex shader
		attribute vec4 _glesVertex;
		attribute vec4 _glesMultiTexCoord0;
		varying mediump vec2 vTextureCoord;
		uniform highp mat4 stMatrix;
		void main() // all vertex shaders define a main() function
		{
			highp vec4 tmpvar_3;
			tmpvar_3.w = 1.0;
			tmpvar_3.xyz = _glesVertex.xyz;
			gl_Position = (unity_MatrixVP * (unity_ObjectToWorld * tmpvar_3));
			vTextureCoord= _glesMultiTexCoord0.xy;
			highp vec4 tmpvar=vec4(vTextureCoord.xy,0.0,1.0);
			vTextureCoord=(stMatrix*tmpvar).xy;
		}

		#endif // here ends the definition of the vertex shader


		#ifdef FRAGMENT // here begins the fragment shader

		#extension GL_OES_EGL_image_external : require
		#extension GL_OES_EGL_image_external_essl3 : require
		precision mediump float;
		varying mediump vec2 vTextureCoord;
		uniform samplerExternalOES _MainTex;
		void main() // all fragment shaders define a main() function
		{
			lowp vec4 tmpvar_1;
			tmpvar_1 = texture2D(_MainTex, vTextureCoord);
			gl_FragData[0] = tmpvar_1;
		}

		#endif // here ends the definition of the fragment shader

		ENDGLSL // here ends the part in GLSL
		}
	}
}