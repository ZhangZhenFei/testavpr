Shader "DpnLauncher/PlainTextureYUV"
{
	Properties
	{
		_MainTex("Texture", 2D) = "white" {}
	}
	SubShader
	{ // Unity chooses the subshader that fits the GPU best
	Cull Off
	ZTest LEqual

	Pass
		{ // some shaders require multiple passes
		GLSLPROGRAM // here begins the part in Unity's GLSL

		#ifdef VERTEX // here begins the vertex shader
		attribute vec4 _glesVertex;
		attribute vec4 _glesMultiTexCoord0;
		uniform highp vec4 _MainTex_ST;
		uniform highp mat4 _RotationMatrix;
		varying mediump vec2 xlv_TEXCOORD0;
		uniform highp mat4 stMatrix;
		void main()
		{
			highp vec2 tmpvar_1;
			mediump vec2 tmpvar_2;
			highp vec4 tmpvar_3;
			tmpvar_3.w = 1.0;
			tmpvar_3.xyz = _glesVertex.xyz;
			tmpvar_1 = (_glesMultiTexCoord0.xy - 0.5);
			highp vec4 tmpvar_4;
			tmpvar_4.zw = vec2(0.0, 1.0);
			tmpvar_4.xy = tmpvar_1;
			tmpvar_1 = (_RotationMatrix * tmpvar_4).xy;
			tmpvar_1 = (tmpvar_1 + 0.5);
			tmpvar_2 = ((tmpvar_1 * _MainTex_ST.xy) + _MainTex_ST.zw);
			gl_Position = (unity_MatrixVP * (unity_ObjectToWorld * tmpvar_3));
			xlv_TEXCOORD0= tmpvar_2.xy;
			//xlv_TEXCOORD0.y = 1.0 - tmpvar_2.y;
			highp vec4 tmpvar_5=vec4(xlv_TEXCOORD0.xy,0.0,1.0);
			xlv_TEXCOORD0 = (stMatrix*tmpvar_5).xy;
		}


		#endif // here ends the definition of the vertex shader


		#ifdef FRAGMENT // here begins the fragment shader

		#extension GL_OES_EGL_image_external : require
		#extension GL_OES_EGL_image_external_essl3 : require
		precision mediump float;
		varying mediump vec2 xlv_TEXCOORD0;
		uniform samplerExternalOES _MainTex;
		void main() // all fragment shaders define a main() function
		{
			lowp vec4 tmpvar_1;
			tmpvar_1 = texture2D(_MainTex, xlv_TEXCOORD0);
			gl_FragData[0] = tmpvar_1;
		}

		#endif // here ends the definition of the fragment shader

		ENDGLSL // here ends the part in GLSL
		}
	}
}
